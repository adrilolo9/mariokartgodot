using Godot;
using System;

public class Main : Spatial
{
    // Declare member variables here. Examples:
    // private int a = 2;
    // private string b = "text";

    // Called when the node enters the scene tree for the first time.
    
    RigidBody mainKart;
    float linearVelocityX = 1500f;
    Random random = new Random();
    private PackedScene EnemyScene = ResourceLoader.Load("res://scenes/Enemy.tscn") as PackedScene;

    public override void _Ready()
    {
        mainKart = GetNode("MainKart") as RigidBody;
        mainKart = (RigidBody) GetNode("MainKart");
    }

    public override void _Input(InputEvent ev){
        if (ev is InputEventKey){
            var key = (InputEventKey) ev;
            if (key.IsPressed() && key.IsAction("ui_left"))
            {
                mainKart.LinearVelocity = new Vector3(-linearVelocityX * GetProcessDeltaTime(), 0, 0);
            }else if(key.IsPressed() && key.IsAction("ui_right")){
                mainKart.LinearVelocity = new Vector3(linearVelocityX * GetProcessDeltaTime(), 0, 0);
            }else{
                mainKart.LinearVelocity = new Vector3(0, 0, 0);
            }
        }
    }

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
//  public override void _Process(float delta)
//  {
//      
//  }

    private void _on_SpawnEnemyTimer_timeout(){
        Godot.RigidBody enemy;
        enemy = EnemyScene.Instance() as Godot.RigidBody;
        
        Godot.Vector3 pos = enemy.Translation;
        pos.z = -500;
        pos.y = 2;
        pos.x = pos.x - (random.Next(-5, 5) * 5);
        enemy.Translation = pos;

        Godot.Vector3 scale = enemy.Scale;
        scale = scale * random.Next(2, 4);
        enemy.Scale = scale;

        AddChild(enemy);
    }
}
